# README #

This project is an implementation of the TicTacToe game.
It includes an artificial player designed to be unbeatable.

This README will walk you through the basic set up of the project. It will also describe the packages and architecture of the application.

### What is this repository for? ###

* The game
* Pre requisites
* Sep up
* Architecture & packages

### The game ###

This is a TicTacToe game playabe through command line on any computer with Java installed.
The game is played against an AI designed to be unbeatable.
The initial turn can be assigned to any player, it is chosen at (pseudo)random.

### Pre requisites ###

If you want to contribute to the project, you should have installed:

1. Java 7 (jre + jdk)
2. Git (recommended Bitbucket account)
3. Maven (recommended 3.2.3 or higher)

### How do I get set up? ###

Clone the repo

```
#!bash

git clone https://bitbucket.org/obprado/tictactoe.git
```
Build the project


```
#!bash

cd tictactoe
mvn clean install
```

Run it!


```
#!bash

cd target
java -jar TicTacToe-1.0
```

### Architecture & packages ###

The software is organized in a [Clean Architecture, as defined by Robert C. Martin.](blog.8thlight.com/uncle-bob/2012/08/13/the-clean-architecture.html)
[It is explained in this video at Ruby Midwest 2011.](https://www.youtube.com/watch?v=WpkDN78P884)

These are the packages:

* Model
    * Game
        * Board
        * Rules
    * Player
        * Choice
        * Search engine
* Application
* UI
    * Terminal

#### Model ####

The model is the innermost layer of the software. It represents the domain knowledge of the TicTacToe game. It has no dependency upon the application layer, the UI layer or any other layer.
Whenever the flow of the program needs the model layer to comunicate with an outer layer, an interface is put between the layers to invert the dependency (for example, the interface com.obprado.tictactoe.model.player.PlayerDisplay inverts the dependency upon the UI layer).

The problem solved in the program has a complex logic. This is implemented in the model layer. Since it is big and complex, it's been divided in we have divided in several sub packages.

The model is implemented in the package with the same name: com.obprado.tictactoe.model
Inside the model we have the game and the player sub packages. 

##### Game #####

Game package contains two sub packages. The entity package contains bussines objects modeling the Board on which the games are played. The services package contains the rules of the game and the logic to decide which player should play at any given moment.

##### Player #####
Player package contains different player implementations. In this case, we have the human player (which has to comunicate with the UI layer) and the AI (an implementation of the [Newell and Simon's 1972 tic-tac-toe program](http://en.wikipedia.org/wiki/Tic-tac-toe#Strategy).

The Choice and the Search engine packages are dependencies of the AI. The Choice package implements all the movements the AI tries to perform when asked for a movement. The Search engine package performs searchs and calculations around the board that are very convenient when implementing the Choice package.

#### Application ####

This package implements the user histories or use cases. It controls the flow of the program, coordinating the players, the Board and the rules to provide a consistent API that represents the application. 

This layer has natural dependencies towards the model layer. Whenever it needs to comunicate with an outer layer (UI layer) it inverts the dependency using the same approach as in the model layer.

This layer is very thin, it only has one use case (playing a TicTacToe game between two players) and factory (which creates a PlayGame with a Human player and an AI).

#### UI (User Interface) ####

The UI is the outermost layer of the application. It contains the different UI implementations. At this moment, the only UI implementation is a command line one. This command line UI lives in the package com.obprado.tictactoe.ui.terminal.


### Schema ###
![TicTacToe architecture - New Page.jpeg](https://bitbucket.org/repo/xe95bx/images/4257334746-TicTacToe%20architecture%20-%20New%20Page.jpeg)