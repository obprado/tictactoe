package com.obprado.tictactoe.application;

import com.obprado.tictactoe.model.game.service.*;
import com.obprado.tictactoe.model.game.entity.*;
import com.obprado.tictactoe.model.player.*;

/**
 *
 * @author omar
 */
public class PlayGame {
    private final Board board;
    private final PlayerDisplay display;
    private TurnManager turnManager;

    PlayGame(PlayerDisplay display, TurnManager manager) {
        this.display = display;
        board = new Board(3);
        this.turnManager = manager;
    }
    
    public void playGame() {
        while( ! isGameOver() )
            makeMovement();
        gameOver();
    }
    
    boolean isGameOver() {
        return new Rules(board).isOver();
    }
    
    void makeMovement() {
        Movement movement = getCurrentTurn().decideMovementFor(board);
        if (valid(movement))
            execute(movement);
        else
            showErrorAndRetry(movement);        
    }
    
    Player getCurrentTurn(){
        return turnManager.getCurrentTurn();
    }
    
    private boolean valid(Movement movement) {
        return new Rules(board).isLegal(movement);
    }
    
    private void execute(Movement movement) {
        board.fill(movement.getPosition(), movement.getPlayer());
        turnManager.notifyA(movement);
        display.notifyMovement();
    }
    
    private void showErrorAndRetry(Movement movement) {
        display.notifyInvalid(movement);
        makeMovement();
    }
    
    void gameOver() {
        display.notifyEndOfGame();
    }
    
    public Classification classificationOf(Player player) {
        return new Rules(board).classificationOf(player);
    }
    
    public Board getBoard() {
        return board;
    }

    public Player getPlayerOne() {
        return turnManager.getFirstPlayer();
    }

    public Player getPlayerTwo() {
        return turnManager.getSecondPlayer();
    }
            
    //For testing purposes. Do not make public
    void setTurnManager(TurnManager turnManager){
        this.turnManager = turnManager;
    }
    
}
