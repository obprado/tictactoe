package com.obprado.tictactoe.application;

import com.obprado.tictactoe.model.game.service.TurnManager;
import com.obprado.tictactoe.model.game.entity.Player;
import com.obprado.tictactoe.model.player.*;

/**
 *
 * @author omar
 */
public class PlayGameFactory {
    
    public static PlayGame getHumanVsMachineGame(Player human, PlayerDisplay display){
        TurnManager turnManager = new TurnManager(
                human, 
                new MinimaxPlayer(human));
        return new PlayGame(display, turnManager); 
    }
    
}
