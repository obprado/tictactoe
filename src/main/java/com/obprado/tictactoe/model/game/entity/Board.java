package com.obprado.tictactoe.model.game.entity;

import java.util.*;

/**
 *
 * @author omar
 */
public class Board{ 
    private final int size;
    private final ArrayList<Player> positions;

    public Board(int size) {
        this.size = size;
        positions = new ArrayList<>(size*size);
        for (int i = 0; i < size*size; i++)
            positions.add(i, null);
    }    
        
    public Player findPlayer(int position) {        
        return positions.get(indexOf(position));
    }
    
    private int indexOf(int position) {
        return position - 1;
    }

    public boolean isFree(int position) {
        return findPlayer(position) == null;
    }
    
    public void fill(int position, Player player){
        positions.set(indexOf(position), player);
    }

    public boolean isFull() {
        boolean isFull = true;
        for (Player position : positions){
            isFull = isFull && position != null;
        }
        return isFull;
    }

    public boolean hasAllInARow() {
        boolean threeInARowFound = false;
        Collection<Collection<Integer>> consecutive = findConsecutivePositions();
        for (Collection<Integer> alignedPositions : consecutive){
            threeInARowFound = threeInARowFound || areAllOwnedByTheSameUser(alignedPositions);
        }
        return threeInARowFound;
    }
    
    private Collection<Collection<Integer>> findConsecutivePositions(){
        Collection<Collection<Integer>> alignedPositions = new ArrayList<>();
        alignedPositions.addAll(rows());
        alignedPositions.addAll(columns());
        alignedPositions.addAll(diagonals());
        return alignedPositions;
    }
    
    private Collection<Collection<Integer>> rows() {
        Collection<Collection<Integer>> rows = new ArrayList<>();
        for (int row = 1; row <= size; row++)
            rows.add(row(row));
        return rows;
    }
    
    private Collection<Integer> row(int row) {
        Collection<Integer> rowPositions = new ArrayList<>();        
        for (int column = 1; column <= size; column++)
            rowPositions.add(calculatePosition(row, column));
        return rowPositions;
    }
    
    public Integer calculatePosition(int row, int column) {
        int rowIndex = row - 1;
        return size*rowIndex + column;
    }
    
    private Collection<Collection<Integer>> columns() {
        Collection<Collection<Integer>> columns = new ArrayList<>();
        for (int column = 1; column <= size; column++)
            columns.add(column(column));
        return columns;
    }
    
    private Collection<Integer> column(int column){
        Collection<Integer> positionsInColumn = new ArrayList<>();
        for (int row = 1; row <= size; row++)
            positionsInColumn.add(calculatePosition(row, column));
        return positionsInColumn;
    }
    
    private Collection<Collection<Integer>> diagonals() {
        Collection<Collection<Integer>> diagonals = new ArrayList<>();
        diagonals.add(ascendentDiagonal());
        diagonals.add(descendentDiagonal());
        return diagonals;
    }
    
    private Collection<Integer> descendentDiagonal() {
        Collection<Integer> positionsInDiagonal = new ArrayList<>();
        for (int rowColumn = 1; rowColumn <= size; rowColumn++)
            positionsInDiagonal.add(calculatePosition(rowColumn, rowColumn));
        return positionsInDiagonal;
    }
    
    private Collection<Integer> ascendentDiagonal() {
        Collection<Integer> positionsInDiagonal = new ArrayList<>();
        for (int column = 1; column <= size; column++)
            positionsInDiagonal.add(calculatePosition(size - (column - 1), column));
        return positionsInDiagonal;
    }

    private boolean areAllOwnedByTheSameUser(Collection<Integer> trio) {
        boolean allOwnedByTheSame = true;
        for (int position : trio)
            allOwnedByTheSame = allOwnedByTheSame && 
                    areOwnedByTheSamePlayer(firstPosition(trio), position);                    
        return allOwnedByTheSame;
    }
    
    private boolean areOwnedByTheSamePlayer(int onePosition, int anotherPosition) {
        Player onePlayer     = findPlayer(onePosition);
        Player anotherPlayer = findPlayer(anotherPosition);
        return (onePlayer != null) && onePlayer.equals(anotherPlayer);
    }
    
    private int firstPosition(Collection<Integer> trio) {
        return trio.iterator().next();
    }

    public boolean hasAllInARowFor(Player player) {
        boolean threeInARowForPlayerFound = false;
        Collection<Collection<Integer>> consecutivePositions = findConsecutivePositions();
        for (Collection<Integer> rowColumnOrDiagonal : consecutivePositions){
            threeInARowForPlayerFound = threeInARowForPlayerFound || 
                    areAllOwnedByTheUser(rowColumnOrDiagonal, player);
        }
        return threeInARowForPlayerFound;
    }

    private boolean areAllOwnedByTheUser(
            Collection<Integer> consecutivePositions, Player player) {
        boolean allOwnedByThePlayer = true;
        for (int position : consecutivePositions){
            allOwnedByThePlayer = allOwnedByThePlayer && 
                                    isPositionOwnedBy(position, player);
        }
        return allOwnedByThePlayer;
    }

    public boolean isPositionOwnedBy(int position, Player player) { 
        Player actualPlayer = findPlayer(position);
        return actualPlayer != null && actualPlayer.equals(player);
    }    

    @Override
    public boolean equals(Object obj) {
        Board other;
        if (obj instanceof Board)
            other = (Board)obj;
        else
            return false;
        return this.areAllThePositionsEqual(other);
    }   

    private boolean areAllThePositionsEqual(Board other) {
        for (int position = 1; position <= positions.size(); position++)
            if ( ! equalPosition(position, other))
                return false;
        return true;
    }
    
    private boolean equalPosition(int position, Board other) {
        if ((findPlayer(position) == null) && (other.findPlayer(position) == null))
            return true;
        if (findPlayer(position) == null)
            return false;
        if (findPlayer(position).equals(other.findPlayer(position)))
            return true;
        return false;
    }
    
    public Collection<Integer> freePositions() {        
        Collection<Integer> freePositions = new ArrayList<>();
        for (int position = 1; position <= positions.size(); position++)
            if (isFree(position))
                freePositions.add(position);
        return freePositions;
    }

    public Board copy(){
        return new Board(this);
    }
    private Board(Board board){
        this.size = board.size;
        this.positions = new ArrayList<>(size*size);
        for (int position = 0; position < this.size*this.size; position++)
            this.positions.add(position, board.findPlayer(position + 1));
    }

    public int getSize() {
        return size;
    }
}
