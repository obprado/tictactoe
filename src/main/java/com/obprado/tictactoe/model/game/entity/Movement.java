package com.obprado.tictactoe.model.game.entity;

/**
 *
 * @author omar
 */
public class Movement {
    private final Player player;
    private final int position;

    public Movement(Player player, int position) {
        this.player = player;
        this.position = position;
    }

    public Player getPlayer() {
        return player;
    }

    public int getPosition() {
        return position;
    }
    
}
