package com.obprado.tictactoe.model.game.entity;

/**
 *
 * @author omar
 */
public interface Player {
    
    public Movement decideMovementFor(Board board);
    
}
