package com.obprado.tictactoe.model.game.service;

/**
 *
 * @author omar
 */
public enum Classification {
    WINNER, LOOSER, UNCLASSIFIED, TIE    
}
