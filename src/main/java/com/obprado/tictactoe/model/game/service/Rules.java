package com.obprado.tictactoe.model.game.service;

import com.obprado.tictactoe.model.game.entity.*;
import static com.obprado.tictactoe.model.game.service.Classification.*;

/**
 *
 * @author omar
 */
public class Rules {
    private final Board board;    

    public Rules(Board board) {
        this.board = board;
    }
    
    public boolean isOver(){
        return board.isFull() || board.hasAllInARow();
    }

    public boolean isLegal(Movement movement) {
        return board.isFree(movement.getPosition());
    }
    
    public Classification classificationOf(Player player) {
        if (! isOver())
            return UNCLASSIFIED;
        if (board.hasAllInARowFor(player))
            return WINNER;
        if (board.hasAllInARow())
            return LOOSER;   
        else
            return TIE;
    }
}
