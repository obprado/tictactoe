package com.obprado.tictactoe.model.game.service;

import com.obprado.tictactoe.model.game.entity.*;
import java.util.*;

/**
 *
 * @author omar
 */
public class TurnManager {
    private final Player firstPlayer;
    private final Player secondPlayer;
    private Random pseudoRandom;
    private Player currentTurn;

    public TurnManager(Player firstPlayer, Player secondPlayer) {
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        initiative();
    }
    
    final void initiative() {       
        if (pseudoRandom == null)
            pseudoRandom = new Random(new Date().getTime());
        if (pseudoRandom.nextBoolean()){
            currentTurn = firstPlayer;
        } else {
            currentTurn = secondPlayer;
        }
    }

    public void notifyA(Movement movement) {
        currentTurn = theOtherPlayer(movement.getPlayer());
    }

    private Player theOtherPlayer(Player currentTurn) {
        if (currentTurn.equals(firstPlayer))
            return secondPlayer;
        else
            return firstPlayer;
    }

    public Player getCurrentTurn() {
        return currentTurn;
    }

    public Player getFirstPlayer() {
        return firstPlayer;
    }

    public Player getSecondPlayer() {
        return secondPlayer;
    }
    
}
