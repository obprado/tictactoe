package com.obprado.tictactoe.model.player;

import com.obprado.tictactoe.model.game.entity.*;

/**
 *
 * @author omar
 */
public class HumanPlayer implements Player {
    private final PlayerDisplay display;

    public HumanPlayer(PlayerDisplay display) {
        this.display = display;
    }    

    @Override
    public Movement decideMovementFor(Board board) {
        return new Movement(this, display.askForAPosition(board));
    }
    
}
