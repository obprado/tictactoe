package com.obprado.tictactoe.model.player;

import com.obprado.tictactoe.model.game.entity.*;
import com.obprado.tictactoe.model.game.service.*;
import java.util.*;

/**
 *
 * @author omar
 */
public class MinimaxPlayer implements Player {
    private final Player opponent;
    public static final String FULL_BOARD = 
            "The minimax player shouldn't be asked for a movement on a full board";
    public static final String GAME_OVER = 
            "The minimax player shouldn't be asked for a movement on a finished game";

    public MinimaxPlayer(Player opponent) {
        this.opponent = opponent;
    }

    @Override
    public Movement decideMovementFor(Board board) {
        return new Movement(this, minimaxInit(board));
    }

    private int minimaxInit(Board board) {
        validate(board);        
        return betterOf(calculateResults(board, this)).getPosition();
    }
    
    private void validate(Board board) {
        if (board.isFull())
            throw new IllegalArgumentException(FULL_BOARD);
        if (new Rules(board).isOver())
            throw new IllegalArgumentException(GAME_OVER);
    }
    
    private Collection<Result> calculateResults(Board board, Player player) {
        Board clonedBoard;
        Collection<Result> results = new ArrayList<>();
        for (int possiblePosition : board.freePositions()) {
            clonedBoard = board.copy();
            clonedBoard.fill(possiblePosition, player);
            results.add(new Result(possiblePosition,
                    minimax(clonedBoard, theOtherPlayer(player))));
        }
        return results;
    }

    private int minimax(Board board, Player player) {  
        if (new Rules(board).isOver())
            return resultToInt(board);
        
        Collection<Result> results = calculateResults(board, player);
        if (player.equals(this))
            return betterOf(results).getResult();
        else
            return worstOf(results).getResult();                    
    }

    private Result betterOf(Collection<Result> results) {
        Result candidate = results.iterator().next();
        for (Result result : results)
            if (result.getResult() > candidate.getResult())
                candidate = result;
        return candidate;
    }
    
    private Result worstOf(Collection<Result> results) {
        Result candidate = results.iterator().next();
        for (Result result : results)
            if (result.getResult() < candidate.getResult())
                candidate = result;
        return candidate;
    }

    private int resultToInt(Board board) {
        Rules rules = new Rules(board);
        if (rules.classificationOf(this).equals(Classification.LOOSER)) {
            return -1;
        }
        if (rules.classificationOf(this).equals(Classification.TIE)) {
            return 0;
        }
        if (rules.classificationOf(this).equals(Classification.WINNER)) {
            return 1;
        }
        throw new RuntimeException("programming error. ResultToInt() shouldn't be invoked if the game have not ended.");
    }

    private Player theOtherPlayer(Player player) {
        if (player.equals(this))
            return opponent;
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof MinimaxPlayer;
    }

    class Result {
        private final int position;
        private final int result;

        public Result(int position, int result) {
            this.position = position;
            this.result = result;
        }

        public int getPosition() {return position;}
        public int getResult() {return result;}
    }

}
