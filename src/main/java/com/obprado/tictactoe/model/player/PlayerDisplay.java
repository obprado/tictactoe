package com.obprado.tictactoe.model.player;

import com.obprado.tictactoe.model.game.entity.*;

/**
 *
 * @author omar
 */
public interface PlayerDisplay {
    
    public int askForAPosition(Board board);    
    public void notifyInvalid(Movement movement);
    public void notifyEndOfGame();
    public void notifyMovement();
}
