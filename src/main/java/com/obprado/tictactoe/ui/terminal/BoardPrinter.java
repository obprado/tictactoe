package com.obprado.tictactoe.ui.terminal;

import java.io.*;
import java.text.MessageFormat;
import com.obprado.tictactoe.model.game.entity.*;

/**
 *
 * @author omar
 */
public class BoardPrinter {
    private final Board board;
    private final PrintStream screen;
    private final PositionPrinter playerOne;
    private final PositionPrinter playerTwo;
    private final PositionPrinter freePosition;
    
    public static final String CEILING = " " + "_____" + " " + "_____" + " " + "_____" + "";
    public static final String SPACING = "|" + "     " + "|" + "     " + "|" + "     " + "|";
    public static final String LINE    = "|" + "  {0}  " + "|" + "  {1}  " + "|" + "  {2}  " + "|";
    public static final String FLOOR   = "|" + "_____" + "|" + "_____" + "|" + "_____" + "|";

    public static final int ROW_UPPER = 1;
    public static final int ROW_CENTRAL = 2;
    public static final int ROW_DOWN = 3;
     public static final int COLUMN_LEFT = 1;
    public static final int COLUMN_CENTRAL = 2;
    public static final int COLUMN_RIGHT = 3;

    public BoardPrinter(Board board, PrintStream screen, 
            PlayerPrinter playerOnePrinter, PlayerPrinter playerTwoPrinter) {
        this.board = board;
        this.screen = screen;
        this.playerOne = playerOnePrinter;
        this.playerTwo = playerTwoPrinter;
        this.freePosition = new FreePositionPrinter();
    }
    
    public void printBoard(){
        printCeiling();
        for (int row = 1; row <= board.getSize(); row++)
            printRow(row);
    }

    private void printCeiling() {
        screen.println(CEILING);
    }
    
    private void printRow(int row){
        printSpacing();
        printLine(row);
        printFloor();
    }
    
    private void printSpacing() {
        screen.println(SPACING);
    }
    
    private void printLine(int row) {        
        screen.println(MessageFormat.format(LINE, 
                fillerFor(board.calculatePosition(row, COLUMN_LEFT)),
                fillerFor(board.calculatePosition(row, COLUMN_CENTRAL)),
                fillerFor(board.calculatePosition(row, COLUMN_RIGHT))
        ));
    }

    private char fillerFor(int position) {
        PositionPrinter printer = findPrinterFor(position);
        return printer.getSymbol();
    }

    private PositionPrinter findPrinterFor(int position) {
        if (freePosition.canPrint(position, board))
            return freePosition;
        if (playerOne.canPrint(position, board))
            return playerOne;
        if (playerTwo.canPrint(position, board))
            return playerTwo;
        throw new RuntimeException("Programming error. No printer found for the position");
    }
    
    private void printFloor() {
        screen.println(FLOOR);
    }    
}
