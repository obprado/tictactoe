package com.obprado.tictactoe.ui.terminal;

import com.obprado.tictactoe.model.game.entity.*;

/**
 *
 * @author omar
 */
public class FreePositionPrinter implements PositionPrinter{

    @Override
    public char getSymbol() {
        return ' ';
    }

    @Override
    public boolean canPrint(int position, Board board) {
        return board.isFree(position);
    }
    
}
