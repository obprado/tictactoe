package com.obprado.tictactoe.ui.terminal;

import java.io.*;

/**
 *
 * @author omar
 */
public class Launcher {
    
    public static void main(String args[]){
        TerminalDisplay display = new TerminalDisplay(
                new BufferedReader(new InputStreamReader(System.in)), 
                System.out);
        display.playHumanVsMachine();
    }
    
}
