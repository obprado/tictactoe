package com.obprado.tictactoe.ui.terminal;

import com.obprado.tictactoe.model.game.entity.*;

/**
 *
 * @author omar
 */
public class PlayerPrinter implements PositionPrinter{
    private final char symbol;
    private final Player player;

    public PlayerPrinter(Player player, char symbol) {
        this.symbol = symbol;
        this.player = player;
    }

    @Override
    public char getSymbol() {
        return symbol;
    }
    
    @Override
    public boolean canPrint(int position, Board board) {
        return board.isPositionOwnedBy(position, player);
    }

    boolean belongsTo(Player other) {
        return this.player.equals(other);
    }

}
