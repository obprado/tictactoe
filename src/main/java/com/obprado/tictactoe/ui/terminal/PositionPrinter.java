package com.obprado.tictactoe.ui.terminal;

import com.obprado.tictactoe.model.game.entity.*;

/**
 *
 * @author omar
 */
public interface PositionPrinter {
    
    public char getSymbol();
    public boolean canPrint(int position, Board board);
    
}
