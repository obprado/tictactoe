package com.obprado.tictactoe.ui.terminal;

import java.io.*;
import java.util.*;
import com.obprado.tictactoe.application.*;
import com.obprado.tictactoe.model.player.*;
import com.obprado.tictactoe.model.game.entity.*;
import com.obprado.tictactoe.model.game.service.Classification;
import static com.obprado.tictactoe.model.game.service.Classification.*;

/**
 *
 * @author omar
 */
public class TerminalDisplay implements PlayerDisplay{
    private final Player human;
    private final PlayGame game;
    private final PrintStream printer;
    private final BufferedReader reader;
    private final BoardPrinter boardPrinter;
    private Map<String, Integer> rowMap;
    private Map<String, Integer> columnMap;
    private Map<String, Integer> positionMap;
    private Map<Classification, String> clasificationMap;

    public static final String REQUEST_POSITION = "Which position do you want to play on? (1 - 9)";
    public static final String INVALID_POSITION = "Please enter a valid position";
    public static final String FREE_POSITION = "Please enter a free position";
    public static final String GAME_OVER = "The game is over";
    public static final String CONGRATULATIONS = "Congratulations, you have won the game!";
    public static final String DEFEAT = "The machine wins. Better luck next time!";
    public static final String DRAW = "You have a Tie with the machine!";
    public static final String KEYBOARD_PROBLEM = "Problem with the keyboard. Check that your keyboard's usb is properly connected to the computer";

    public TerminalDisplay(BufferedReader reader, PrintStream printer) {
        this.human = new HumanPlayer(this);
        this.game = PlayGameFactory.getHumanVsMachineGame(human, this);
        this.reader = reader;
        this.printer = printer;
        this.boardPrinter = new BoardPrinter(
                game.getBoard(), 
                printer, 
                new PlayerPrinter(game.getPlayerOne(), 'X'), 
                new PlayerPrinter(game.getPlayerTwo(), 'O'));        
        initializePositions();
        initializeClasifications();
    }   
    
    private void initializePositions() {
        positionMap = new HashMap<>();
        positionMap.put("1", 1);
        positionMap.put("2", 2);
        positionMap.put("3", 3);
        positionMap.put("4", 4);
        positionMap.put("5", 5);
        positionMap.put("6", 6);
        positionMap.put("7", 7);
        positionMap.put("8", 8);
        positionMap.put("9", 9);
    }
    
    private void initializeClasifications() {
        clasificationMap = new HashMap<>();
        clasificationMap.put(WINNER, CONGRATULATIONS);
        clasificationMap.put(LOOSER, DEFEAT);
        clasificationMap.put(TIE, DRAW);
    }
    
    void playHumanVsMachine() {
        displayGameBeginning();
        game.playGame();        
    }
    
    private void displayGameBeginning() {
        boardPrinter.printBoard();
    }

    @Override
    public int askForAPosition(Board board) {
        try {                     
            printer.println(REQUEST_POSITION);
            String position = reader.readLine();      
            if (positionMap.containsKey(position))
                return positionMap.get(position);
            else {
                printer.println(INVALID_POSITION);
                return askForAPosition(board);
            }
        } catch (IOException exception) {
            printer.println(KEYBOARD_PROBLEM);
            return askForAPosition(board);
        }
    }

    @Override
    public void notifyInvalid(Movement movement) {
        printer.println(FREE_POSITION);
    }

    @Override
    public void notifyEndOfGame() {
        printer.println(GAME_OVER);
        printClasification();
    }
    
    private void printClasification() {
        printer.println(
                clasificationMap.get(
                        findClasification()));        
    }
    
    private Classification findClasification() {
        return game.classificationOf(getPlayer());
    }
    
    @Override
    public void notifyMovement() {
        boardPrinter.printBoard();
    }

    Player getPlayer() {
        return human;
    }      
    
    //for testing purposes. Do not make public
    TerminalDisplay(PlayGame game, PrintStream printer, BufferedReader reader) {
        this.human = new HumanPlayer(this);
        this.game = game;
        this.printer = printer;
        this.reader = reader;        
        this.boardPrinter = new BoardPrinter(
                game.getBoard(), 
                printer,
                new PlayerPrinter(game.getPlayerOne(), 'X'), 
                new PlayerPrinter(game.getPlayerTwo(), 'O'));
        initializePositions();
        initializeClasifications();
    }

    
    
}
