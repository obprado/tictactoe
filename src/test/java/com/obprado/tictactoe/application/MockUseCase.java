package com.obprado.tictactoe.application;

import com.obprado.tictactoe.model.player.*;
import com.obprado.tictactoe.model.game.entity.*;
import com.obprado.tictactoe.model.game.service.*;
import static com.obprado.tictactoe.model.game.TestFactories.*;

/**
 *
 * @author omar
 */
public class MockUseCase extends PlayGame{
    private Classification clasification;
    private Board board;

    public MockUseCase(PlayerDisplay display, TurnManager manager) {
        super(display, manager);
    }

    @Override
    public Board getBoard() {
        return board;
    }

    @Override
    public Classification classificationOf(Player player) {
        return clasification;
    }
    
    public Player getHumanPlayer() {
        return somePlayer();
    }

    public Classification getClasification() {
        return clasification;
    }

    public void setClasification(Classification clasification) {
        this.clasification = clasification;
    }

    public void setBoard(Board board) {
        this.board = board;
    }
    
}