package com.obprado.tictactoe.application;

import java.util.*;
import com.obprado.tictactoe.model.player.*;
import com.obprado.tictactoe.model.game.entity.*;
import com.obprado.tictactoe.model.game.service.*;
import static com.obprado.tictactoe.model.game.service.Classification.*;
import static com.obprado.tictactoe.model.game.TestFactories.*;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author omar
 */
public class PlayGameTest {
    private PlayGame useCase;
    private Player currentPlayer;
    private MockDisplay display;
    
    @Test
    public void shouldCreateAnEmptyBoard(){
        given(aHumanVsMachineMatch(ofSomePlayer()));
        thenItShouldHaveAnEmptyBoard();
    }
    
    @Test
    public void shouldChangeTheBoardOnEachMovement(){
        given(aHumanVsMachineMatch(ofSomePlayer()));
        whenAMovementIsMade();
        thenTheBoardShouldNotBe(anEmptyBoard(sized(3)));
    }
    
    @Test
    public void shouldExecuteTheMovementProvidedByThePlayer(){
        given(aDisplay());
        given(aHumanVsMachineMatch(display));
        givenThatTheGameHas(aHumanTurnManager(theHumanPlayer()));
        given(theUseCaseCurrentTurn());
        whenAMovementIsMade();
        thenTheBoardShouldHaveBeenPlayedWith(
                aMovement(display.firstPosition(), currentPlayer));
    }
    
    @Test
    public void shouldChangeTheCurrentPlayerAfterExecutingAMovement(){
        given(aHumanVsMachineMatch(ofSomePlayer()));
        given(theUseCaseCurrentTurn());
        whenAMovementIsMade();
        thenThereShouldBeADifferent(currentPlayer);
    }
    
    @Test
    public void shouldDetectInvalidMovementsAndNotifyTheDisplay(){
        given(aDisplay());
        given(aHumanVsMachineMatch(display));
        givenThatTheGameHas(aHumanTurnManager(theHumanPlayer()));
        givenThatWeHaveAlreadyFilled(theUseCaseBoard(), withAMovement());
        whenAMovementIsMade();
        thenTheDisplayShouldBeInformedAboutTheInvalidMovement();
    }
    
    @Test
    public void shouldRetryTheMovementWhenInvalid(){
        given(aDisplay());
        given(aHumanVsMachineMatch(display));
        givenThatTheGameHas(aHumanTurnManager(theHumanPlayer()));
        givenThatWeHaveAlreadyFilled(theUseCaseBoard(), withAMovement());
        whenAMovementIsMade();
        thenItShouldAskForAMovementAgain();
    }
    
    @Test
    public void shouldNotifyTheDisplayWhenTheGameIsOver(){
        given(aDisplay());
        given(aHumanVsMachineMatch(display));
        whenTheGameIsOver();
        thenTheDisplayShouldBeInformedAboutTheEndOfTheGame();
    }
    
    @Test
    public void shouldBeAbleToPlayAnEntireGame(){
        given(aFreePositionDisplay());
        given(aHumanVsMachineMatch(display));
        whenTheGameIsPlayed();
        thenTheGameShouldBeOver();
    }
    
    @Test
    public void shouldNotifyWhenTheGameIsOver(){
        given(aFreePositionDisplay());
        given(aHumanVsMachineMatch(display));
        whenTheGameIsPlayed();
        thenTheDisplayShouldBeInformedAboutTheEndOfTheGame();
    }
    
    @Test
    public void shouldKnowWhenTheUserHaveWon(){
        given(aDisplay());
        given(aHumanThatInteractsThroughA(display));
        given(aHumanVsMachineMatch(display));
        givenThatWeHaveAlreadyFilled(theUseCaseBoard(), 
                aWinnerCombo(theHumanPlayer()));
        thenTheGameShouldDeclare(theHumanPlayer(), WINNER);
    }
    
    @Test
    public void shouldNotifyTheDisplayWhenAMovementIsExecuted(){
        given(aDisplay());
        given(aHumanVsMachineMatch(display));
        whenAMovementIsMade();
        thenTheDisplayShouldBeInformedAboutTheMovement();
    }
    
    private void given(MockDisplay display) {
        this.display = display;
    }

    private void given(PlayGame useCase) {
        this.useCase = useCase;
    }
    
    private void given(Player currentTurn) {
        this.currentPlayer = currentTurn;
    }
    
    private Player theUseCaseCurrentTurn(){
        return this.useCase.getCurrentTurn();
    }

    private void thenItShouldHaveAnEmptyBoard() {
        assertEquals("The game should be created with an empty board", 
                anEmptyBoard(sized(3)), useCase.getBoard());
    }

    private void whenAMovementIsMade() {
        useCase.makeMovement();
    }
    
    private void thenTheBoardShouldNotBe(Board unexpected) {
        assertNotEquals("The board should be different", 
                unexpected, useCase.getBoard());
    }

    private void thenTheBoardShouldHaveBeenPlayedWith(Movement expected) {
        assertTrue("The board should have been played with the position provided by the display", 
                useCase.getBoard().
                        isPositionOwnedBy(
                                expected.getPosition(), 
                                expected.getPlayer()));
    }

    private void thenThereShouldBeADifferent(Player unexpectedCurrentPlayer) {
        assertNotEquals("The player should have changed", 
                unexpectedCurrentPlayer, theUseCaseCurrentTurn());
    }
    
    private Board theUseCaseBoard(){
        return this.useCase.getBoard();
    }
    
    private Movement withAMovement(){
        return new HumanPlayer(new MockDisplay()).
                decideMovementFor(theUseCaseBoard());        
    }

    private void givenThatWeHaveAlreadyFilled(Board board, Movement movement) {
        board.fill(movement.getPosition(), movement.getPlayer());
    }
    
    private void givenThatWeHaveAlreadyFilled(
            Board board, Collection<Movement> movements) {
        for (Movement movement : movements)
            board.fill(movement.getPosition(), movement.getPlayer());
    }

    private void thenTheDisplayShouldBeInformedAboutTheInvalidMovement() {
        assertTrue("The display should be informed about the invalid movement", 
                display.notifiedInvalid());
    }

    private void givenThatTheGameHas(HumanTurnManager anAITurnManager) {
        useCase.setTurnManager(anAITurnManager);
    }

    private void thenItShouldAskForAMovementAgain() {
        assertEquals("The game should have asked for a movement again", 
                2, display.timesAskedForAPosition());
    }

    private void whenTheGameIsOver() {
        useCase.gameOver();
    }

    private void thenTheDisplayShouldBeInformedAboutTheEndOfTheGame() {
        assertTrue("The game should notify the display about the end of the game",
                display.notifiedEndOfGame());
    }

    private void whenTheGameIsPlayed() {
        useCase.playGame();
    }

    private void thenTheGameShouldBeOver() {
        assertTrue("The game should be over", useCase.isGameOver());
    }

    private Collection<Movement> aWinnerCombo(Player player) {
        Collection<Movement> movements = new ArrayList<>();
        for (int position : alignedTrio())
            movements.add(aMovement(position, player));
        return movements;
    }

    private Player theHumanPlayer() {
        if (useCase.getPlayerOne() instanceof HumanPlayer)
            return useCase.getPlayerOne();
        else
            return useCase.getPlayerTwo();
    }

    private void thenTheGameShouldDeclare(Player player, Classification expected) {
        assertEquals("The game should know the right clasification of the player", 
                expected, useCase.classificationOf(player));
    }

    private void thenTheDisplayShouldBeInformedAboutTheMovement() {
        assertTrue("The display should be informed about the movement",
                display.isNotifiedMovement());
    }
}
