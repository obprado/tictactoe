package com.obprado.tictactoe.model.game;

import com.obprado.tictactoe.model.game.service.*;
import com.obprado.tictactoe.model.game.entity.*;
import com.obprado.tictactoe.application.*;
import static com.obprado.tictactoe.model.game.service.Classification.*;
import com.obprado.tictactoe.model.player.*;
import com.obprado.tictactoe.ui.terminal.*;
import java.util.*;

/**
 *
 * @author omar
 */
public class TestFactories {
    public static final int ROW_UPPER = 1;
    public static final int ROW_CENTER = 2;
    public static final int ROW_DOWN = 3;
    public static final int COLUMN_LEFT = 1;
    public static final int COLUMN_CENTER = 2;
    public static final int COLUMN_RIGHT = 3;

    public static final int DEFAULT_SIZE = 3;
    
    public static Board anEmptyBoard(int size) {
        return new Board(size);
    }
    
    public static Collection<Integer> allThePositions() {
        return allThePositions(DEFAULT_SIZE);
    }
    
    public static Collection<Integer> allThePositions(int boardSize) {
        Collection<Integer> positions = new ArrayList<>();
        for (int position = 1; position <= boardSize*boardSize; position++)
            positions.add(position);
        return positions;
    }
    
    public static Player ofSomePlayer() {
        return bySomePlayer();
    }
    
    public static Player somePlayer() {
        return bySomePlayer();
    }
    
    public static Player bySomePlayer() {
        return new MockPlayer("some player");
    }
    
    public static Player byADifferentPlayer() {
        return new MockPlayer("different player");
    }
    
    public static Player andADifferentPlayer() {
        return byADifferentPlayer();
    }
    
    public static Player byAnOpponent() {
        return new MockPlayer("opponent");
    }
    
    public static Collection<Integer> allPositionsExcept(Integer onePosition) {
        Collection<Integer> positions = allThePositions();
        positions.remove(onePosition);
        return positions;
    }
    
    public static Collection<Integer> allPositionsExcept(
            Collection<Integer> excludedPositions) {
        Collection<Integer> positions = allThePositions();
        for (Integer excludedPosition : excludedPositions)
            positions.remove(excludedPosition);
        return positions;
    }
    
    public static boolean isContainedIn(int position, Collection<Integer> positions){
        return positions.contains(position);
    }
     
    public static Collection<Integer> alignedTrio() {
        ArrayList<Integer> row = new ArrayList<>();
        row.add(aPosition(ROW_DOWN, COLUMN_LEFT));
        row.add(aPosition(ROW_DOWN, COLUMN_CENTER));
        row.add(aPosition(ROW_DOWN, COLUMN_RIGHT));
        return row;
    }
    
    public static int aPosition(int row, int column) {
        return (3*(row - 1))+column;        
    }
    
    public static Collection<Integer> twoConsecutivePositions(){
        Collection<Integer> positions = new ArrayList<>();
        positions.add(aPosition(ROW_DOWN, COLUMN_LEFT));
        positions.add(aPosition(ROW_DOWN, COLUMN_CENTER));
        return positions;
    }
    
    public static Integer theThirdConsecutivePosition(){
        return aPosition(ROW_DOWN, COLUMN_RIGHT);
    }
    
    public static Collection<Integer> upperRow(){
        Collection<Integer> row = new ArrayList<>();
        row.add(aPosition(ROW_UPPER, COLUMN_LEFT));
        row.add(aPosition(ROW_UPPER, COLUMN_CENTER));
        row.add(aPosition(ROW_UPPER, COLUMN_RIGHT));
        return row;
    }
    
    public static Collection<Integer> centerRow(){
        Collection<Integer> row = new ArrayList<>();
        row.add(aPosition(ROW_CENTER, COLUMN_LEFT));
        row.add(aPosition(ROW_CENTER, COLUMN_CENTER));
        row.add(aPosition(ROW_CENTER, COLUMN_RIGHT));
        return row;
    }
    
    public static Collection<Integer> downRow(){
        Collection<Integer> row = new ArrayList<>();
        row.add(aPosition(ROW_DOWN, COLUMN_LEFT));
        row.add(aPosition(ROW_DOWN, COLUMN_CENTER));
        row.add(aPosition(ROW_DOWN, COLUMN_RIGHT));
        return row;
    }
    
    
    public static int aThousandTimes(){
        return 1000;
    }
    
    public static int fourHundredTimes(){
        return 400;
    }
    
    public static int sixHundredTimes(){
        return 600;
    }    
    
    public static int oneTime(){
        return 1;
    }
    
    public static Collection<Integer> allTheCorners(){
        Collection<Integer> corners = new ArrayList<>();
        corners.add(aPosition(ROW_DOWN, COLUMN_LEFT));
        corners.add(aPosition(ROW_DOWN, COLUMN_RIGHT));
        corners.add(aPosition(ROW_UPPER, COLUMN_LEFT));
        corners.add(aPosition(ROW_UPPER, COLUMN_RIGHT));
        return corners;
    }
    
    public static Collection<Integer> twoAscendentCorners(){
        Collection<Integer> opposedCorners = new ArrayList<>();
        opposedCorners.add(aPosition(ROW_DOWN, COLUMN_LEFT));
        opposedCorners.add(aPosition(ROW_UPPER, COLUMN_RIGHT));
        return opposedCorners;
    }
    
    public static Collection<Integer> twoDescendentCorners(){
        Collection<Integer> opposedCorners = new ArrayList<>();
        opposedCorners.add(aPosition(ROW_UPPER, COLUMN_LEFT));
        opposedCorners.add(aPosition(ROW_DOWN, COLUMN_RIGHT));
        return opposedCorners;
    }
    
    public static Collection<Integer> twoPositionsInAColumn(int column) {
        Collection<Integer> partialColumn = new ArrayList<>();
        partialColumn.add(aPosition(ROW_DOWN, column));
        partialColumn.add(aPosition(ROW_UPPER, column));
        return partialColumn;
    }
    
    public static Collection<Integer> cornersInRow(int row) {
        Collection<Integer> corners = new ArrayList<>();
        corners.add(aPosition(row, COLUMN_LEFT));
        corners.add(aPosition(row, COLUMN_RIGHT));
        return corners;
    }
    
    public static Collection<Integer> cornersInColumn(int column) {
        Collection<Integer> corners = new ArrayList<>();
        corners.add(aPosition(ROW_DOWN, column));
        corners.add(aPosition(ROW_UPPER, column));
        return corners;
    }
    
    public static Collection<Integer> theRestOfTheDiagonal(){
        Collection<Integer> restOfDiagonal = new ArrayList<>();
        restOfDiagonal.add(aPosition(ROW_DOWN, COLUMN_LEFT));
        restOfDiagonal.add(aPosition(ROW_CENTER, COLUMN_CENTER));
        return restOfDiagonal;
    }
    
    public static Collection<Integer> cornerAndOpposedSide() {
        Collection<Integer> opposedCorners = new ArrayList<>();
        opposedCorners.add(aPosition(ROW_DOWN, COLUMN_LEFT));
        opposedCorners.add(aPosition(ROW_CENTER, COLUMN_RIGHT));
        return opposedCorners;
    }
    
    public static Collection<Integer> notOpposedSides() {
        Collection<Integer> sides = new ArrayList<>();
        sides.add(aPosition(ROW_CENTER, COLUMN_LEFT));
        sides.add(aPosition(ROW_DOWN, COLUMN_CENTER));
        return sides;
    }
    
    public static int theCornerBetweenTheSides(){
        return aPosition(ROW_DOWN, COLUMN_LEFT);
    }
    
    public static Collection<Integer> cornerAndOpposedSides() {
        Collection<Integer> cornerAndSides = new ArrayList<>();
        cornerAndSides.add(aPosition(ROW_DOWN, COLUMN_LEFT));
        cornerAndSides.add(aPosition(ROW_CENTER, COLUMN_RIGHT));
        cornerAndSides.add(aPosition(ROW_UPPER, COLUMN_CENTER));
        return cornerAndSides;
    }
    
    public static Collection<Integer> allTheSides() {
        Collection<Integer> sides = new ArrayList<>();
        sides.add(aPosition(ROW_DOWN, COLUMN_CENTER));
        sides.add(aPosition(ROW_CENTER, COLUMN_LEFT));
        sides.add(aPosition(ROW_CENTER, COLUMN_RIGHT));
        sides.add(aPosition(ROW_UPPER, COLUMN_CENTER));
        return sides;
    }
    
    public static Collection<Integer> centerAndCorners() { 
        Collection<Integer> centerAndCorners = allTheCorners();
        centerAndCorners.add(theCenter());
        return centerAndCorners;
    }
        
    public static Collection<Integer> topAndCenter() { 
        Collection<Integer> positions = new ArrayList<>();
        positions.add(aPosition(ROW_UPPER, COLUMN_CENTER));
        positions.add(theCenter());
        return positions;
    }    
    
    public static Collection<Integer> centerAndDownOf(int column){
        Collection<Integer> partialColumn = new ArrayList<>();
        partialColumn.add(aPosition(ROW_DOWN, column));
        partialColumn.add(aPosition(ROW_CENTER, column));
        return partialColumn;
    }
    
    public static int thirdPositionInAColumn(int column) {
        return aPosition(ROW_CENTER, column);
    }    
    
    public static int theCenter() {
        return aPosition(ROW_CENTER, COLUMN_CENTER);
    }
    
    public static Rules theRules(Board board) {
        return new Rules(board);
    }
    
    public static Movement aMovement(int position, Player player) {
        return new Movement(player, position);
    }
    
    public static Classification aWinner() {
        return WINNER;
    }
    
    public static Classification aLooser() {
        return LOOSER;
    }
    
    public static Classification anUnclasified() {
        return UNCLASSIFIED;
    }
    
    public static Classification aTie(){
        return TIE;
    }    
    
    public static TurnManager aTurnManager(Player onePlayer, Player anotherPlayer){
        return new TurnManager(onePlayer, anotherPlayer);
    }
    
    public static HumanTurnManager aHumanTurnManager(Player human){
        return new HumanTurnManager(human);
    }
    
    public static PlayGame aHumanVsMachineMatch(Player human){
        return PlayGameFactory.getHumanVsMachineGame(human, aDisplay());
    }
    
    public static PlayGame aHumanVsMachineMatch(PlayerDisplay display){
        return PlayGameFactory.getHumanVsMachineGame(aHumanThatInteractsThroughA(display), display);
    }
    
    public static MockDisplay aDisplay(){
        return new MockDisplay();
    }
    
    public static HumanPlayer aHumanThatInteractsThroughA(PlayerDisplay display){
        return new HumanPlayer(display);
    }
    
    public static FreePositionDisplay aFreePositionDisplay(){
        return new FreePositionDisplay();
    }
    
    public static PlayerPrinter aPlayerPrinter(Player player, char symbol){
        return new PlayerPrinter(player, symbol);
    }
    
    public static MockScreen aScreen() {
        return new MockScreen(System.out);
    }
     
    public static int sized(int size) {
        return size;
    }
    
}
