package com.obprado.tictactoe.model.game.entity;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.*;
import static com.obprado.tictactoe.model.game.TestFactories.*;

/**
 *
 * @author omar
 */
public class BoardTest {
    private Board board;
    private int position;
    private Collection<Integer> emptyPositions;
    private Board clonedBoard;

    @Test
    public void shouldFindPlayersFromPosition() {
        given(anEmptyBoard(sized(3)));
        thenItShouldConsiderValid(allThePositions());
    }
    
    @Test
    public void shouldBeCreatedWithNinePositions() {
        given(anEmptyBoard(sized(3)));
        thenItShouldHaveFree(allThePositions());
    }

    @Test
    public void shouldFillAPosition() {
        given(aPosition(ROW_DOWN, COLUMN_LEFT));
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(position, bySomePlayer());
        thenItShouldNotHaveFree(position);
    }

    @Test
    public void shouldNotFillOtherPositions() {
        given(aPosition(ROW_DOWN, COLUMN_LEFT));
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(position, bySomePlayer());
        thenItShouldHaveFree(allPositionsExcept(position));
    }

    @Test
    public void shouldKnowWhenItIsNotFull() {
        given(anEmptyBoard(sized(3)));
        thenItShouldNotBeFull();
    }

    @Test
    public void shouldNotConfuseBeingFullWithHavingOneFullPosition() {
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(theCenter(),
                        bySomePlayer());
        thenItShouldNotBeFull();
    }

    @Test
    public void shouldKnowWhenItIsFull() {
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(allThePositions(), bySomePlayer());
        thenItShouldBeFull();
    }

    @Test
    public void shouldKnowWhenItHasThreeAlignedPositionsWithSamePlayer() {
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(alignedTrio(), bySomePlayer());
        thenItShouldHaveAllInARow();
    }

    @Test
    public void shouldKnowIfAParticularPlayerHasThreeInARow() {
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(alignedTrio(), bySomePlayer());
        thenItShouldHaveAllInARowFor(bySomePlayer());
    }
    
    @Test
    public void shouldKnowWhenAParticularPlayerDoNotHaveThreeInARow() {
        given(aPosition(ROW_DOWN, COLUMN_LEFT));
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(position, bySomePlayer());
        thenItShouldNotHaveAllInARowFor(bySomePlayer());
    }

    @Test
    public void shouldKnowIfAPositionIsOwnedByAPlayer() {
        given(aPosition(ROW_DOWN, COLUMN_LEFT));
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(position, bySomePlayer());
        thenItShouldBeOwnedIn(position, bySomePlayer());
    }

    @Test
    public void shouldNotRecognizePlayerOwnershipOverAFreePosition() {
        given(anEmptyBoard(sized(3)));
        thenItShouldNotBeOwnedIn(
                aPosition(ROW_DOWN, COLUMN_LEFT),
                bySomePlayer());
    }

    @Test
    public void shouldNotRecognizePlayerOwnershipOverAPositionOwnedByADifferentPlayer() {
        given(aPosition(ROW_DOWN, COLUMN_LEFT));
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(position, bySomePlayer());
        thenItShouldNotBeOwnedIn(position, new MockPlayer("a different player"));
    }

    @Test
    public void shouldConsiderEqualTwoEmptyBoards() {
        given(anEmptyBoard(sized(3)));
        thenItShouldBeEqualTo(anEmptyBoard(sized(3)));
    }

    @Test
    public void shouldNotConsiderEqualANonBoardObject() {
        given(anEmptyBoard(sized(3)));
        thenItShouldNotBeEqualTo(new Object());
    }

    @Test
    public void shouldConsiderEqualTwoBoardsWithTheSameContent() {
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(aPosition(ROW_DOWN, COLUMN_LEFT), bySomePlayer()).
                andAlsoIn(theCenter(), byAnOpponent());
        thenItShouldBeEqualTo(aSimilarBoard());
    }
    
    @Test
    public void shouldFindEmptyPositionsOnAnEmptyBoard(){
        given(anEmptyBoard(sized(3)));
        whenAskedForTheEmptyPositions();
        thenItShouldReturn(allThePositions());
    }
    
    @Test
    public void shouldFindEmptyPositionsOnAHalfFilledBoard(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(allPositionsExcept(theCenter()), bySomePlayer());
        whenAskedForTheEmptyPositions();
        thenItShouldReturn(theCenter());
    }
    
    @Test
    public void shouldNotFindEmptyPositionsOnFilledBoard(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(allThePositions(), bySomePlayer());
        whenAskedForTheEmptyPositions();
        thenThereShouldBeNoEmptyPositions();
    }
    
    @Test
    public void shouldNotAddElementsAfterCloned(){
        given(anEmptyBoard(sized(3)));
        whenTheBoardIsCloned();
        whenTheClonedBoardIsFilledIn(theCenter(), bySomePlayer());
        thenTheBoardShouldStillBeEmpty();
    }
    
    @Test
    public void shouldKeepAllTheElementsAfterCloned(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(allTheCorners(), bySomePlayer());
        whenTheBoardIsCloned();
        thenTheClonedBoardShouldBeOwnedIn(allTheCorners(), bySomePlayer());
    }
    
    private BoardTest given(Board board) {
        this.board = board;
        return this;
    }

    private BoardTest given(int position) {
        this.position = position;
        return this;
    }

    private BoardTest whichHasBeenFilledIn(int position, Player player) {
        return whichHasBeenFilledIn(new ArrayList(Arrays.asList(position)), player);
    }

    private BoardTest whichHasBeenFilledIn(Collection<Integer> positions, Player player) {
        for (int currentPosition : positions)
            board.fill(currentPosition, player);
        return this;
    }

    private void andAlsoIn(int position, Player player) {
        whichHasBeenFilledIn(position, player);
    }
    
    private void thenItShouldHaveFree(Collection<Integer> freePositions) {
        for (int currentPosition : freePositions)
            assertTrue("The position should be free", this.board.isFree(currentPosition));        
    }

    private void thenItShouldNotHaveFree(int position) {
        assertFalse("The board position shouldn't be free", board.isFree(position));
    }

    private void thenItShouldNotBeFull() {
        assertFalse("The board shouldn't be full", board.isFull());
    }

    private void thenItShouldBeFull() {
        assertTrue("The board should be full", board.isFull());
    }

    private void thenItShouldHaveAllInARow() {
        assertTrue("The board should have three in a row", board.hasAllInARow());
    }

    private void thenItShouldHaveAllInARowFor(Player player) {
        assertTrue("The board should know it has three in a row for the player",
                board.hasAllInARowFor(player));
    }

    private void thenItShouldNotHaveAllInARowFor(Player player) {
        assertFalse("The board should know it don't have three in a row for the player",
                board.hasAllInARowFor(player));
    }

    private void thenTheClonedBoardShouldBeOwnedIn(Collection<Integer> positions, Player player) {
        for (int ownedPosition : positions)
            assertTrue("The position should be owned by the player",
                clonedBoard.isPositionOwnedBy(ownedPosition, player));
    }
    
    private void thenItShouldBeOwnedIn(int position, Player player) {
        assertTrue("The position should be owned by the player",
                board.isPositionOwnedBy(position, player));
    }

    private void thenItShouldNotBeOwnedIn(int position, Player player) {
        assertFalse("The position should not be owned by the player",
                board.isPositionOwnedBy(position, player));
    }

    private void thenItShouldBeEqualTo(Board other) {
        assertTrue("The board should know when it is equal to another board",
                board.equals(other));
    }

    private void thenItShouldNotBeEqualTo(Object another) {
        assertFalse("The board should know when it isn't equal to another",
                board.equals(another));
    }

    private Board aSimilarBoard() {
        Board similarBoard = new Board(3);
        similarBoard.fill(aPosition(ROW_DOWN, COLUMN_LEFT), bySomePlayer());
        similarBoard.fill(theCenter(), byAnOpponent());
        return similarBoard;
    }

    private void whenAskedForTheEmptyPositions() {
        emptyPositions = board.freePositions();
    }

    private void thenItShouldReturn(Collection<Integer> expected) {
        for (int expectedPosition : expected)
            assertTrue("The position should have been found as empty", 
                    emptyPositions.contains(expectedPosition));
    }
    
    private void thenItShouldReturn(int expected) {        
        assertTrue("The position should have been found as empty", 
                    emptyPositions.contains(expected));
    }

    private void whenTheBoardIsCloned() {
       clonedBoard = board.copy();
    }

    private void whenTheClonedBoardIsFilledIn(int position, Player owner) {
        clonedBoard.fill(position, owner);
    }

    private void thenTheBoardShouldStillBeEmpty() {
        assertEquals("The board should be empty", 9, board.freePositions().size());
    }

    private void thenItShouldConsiderValid(Collection<Integer> somePositions) {
        try {
            for (int currentPosition : somePositions)
                board.findPlayer(currentPosition);
        } catch(Exception exception){
            fail("The board should find a cell for each position");
        }
    }

    private void thenThereShouldBeNoEmptyPositions() {
        assertTrue("There should be no empty positions", emptyPositions.isEmpty());
    }
    
}
