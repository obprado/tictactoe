package com.obprado.tictactoe.model.game.entity;

import static com.obprado.tictactoe.model.game.TestFactories.*;
/**
 *
 * @author omar
 */
public class MockPlayer implements Player {    
    private final String mockId;

    public MockPlayer() {
        this.mockId = "";
    }

    public MockPlayer(String mockId) {
        this.mockId = mockId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (! (obj instanceof MockPlayer))
            return false;
        MockPlayer other = (MockPlayer)obj;
        return other.mockId.equals(this.mockId);
    }

    @Override
    public Movement decideMovementFor(Board board) {
        return new Movement(this, aPosition(ROW_DOWN, COLUMN_LEFT));
    }
    
}
