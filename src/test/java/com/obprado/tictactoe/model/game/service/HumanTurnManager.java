package com.obprado.tictactoe.model.game.service;

import com.obprado.tictactoe.model.game.entity.Player;

/**
 *
 * @author omar
 */
public class HumanTurnManager extends TurnManager{
    private final Player human;

    public HumanTurnManager(Player human) {
        super(human, human);
        this.human = human;
    }

    @Override
    public Player getCurrentTurn() {
        return human;
    }
    
}
