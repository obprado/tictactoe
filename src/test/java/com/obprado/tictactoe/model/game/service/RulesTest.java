package com.obprado.tictactoe.model.game.service;

import com.obprado.tictactoe.model.game.entity.*;
import static com.obprado.tictactoe.model.game.TestFactories.*;
import java.util.*;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author omar
 */
public class RulesTest {
    private Board board;
    private Rules rules;
    private Movement movement;
    
    @Test
    public void shouldKnowWhenTheGameIsOver(){        
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(allThePositions(), bySomePlayer());
        given(theRules(board));
        thenTheRulesShouldKnowTheGameIsOver();
    }
    
    @Test
    public void shouldKnowWhenTheGameIsNotOver(){
        given(anEmptyBoard(sized(3)));
        given(theRules(board));
        thenTheRulesShouldKnowTheGameIsNotOver();
    }
    
    @Test
    public void shouldKnowTheGameCanBeOverBeforeTheBoardIsFull(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(alignedTrio(), bySomePlayer());
        given(theRules(board));
        thenTheRulesShouldKnowTheGameIsOver();
    }
    
    @Test
    public void shouldKnowWhenAMovementIsLegal(){
        given(anEmptyBoard(sized(3)));
        given(theRules(board));
        given(aMovement(theCenter(), bySomePlayer()));
        thenTheRulesShouldKnowTheMovementIsLegal();
    }
    
    @Test
    public void shouldKnowWhenAMovementIsNotLegal(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(theCenter(), bySomePlayer());
        given(theRules(board));
        given(aMovement(theCenter(), bySomePlayer()));
        thenTheRulesShouldKnowTheMovementIsNotLegal();
    }
    
    @Test
    public void shouldKnowWhenAPlayerHaveWonTheGame(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(alignedTrio(), bySomePlayer());
        given(theRules(board));
        thenTheRulesShouldDeclare(somePlayer(), aWinner());
    }

    @Test
    public void shouldKnowWhenAPlayerHaveLostTheGame(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(alignedTrio(), byAnOpponent());
        given(theRules(board));
        thenTheRulesShouldDeclare(somePlayer(), aLooser());
    }
    
    @Test
    public void shouldKnowAPlayerIsUnclasifiedBeforeTheGameEnds(){
        given(anEmptyBoard(sized(3)));
        given(theRules(board));
        thenTheRulesShouldDeclare(somePlayer(), anUnclasified());
    }
    
    @Test
    public void shouldKnowWhenAPlayerIsInATie(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(tiePositionsForThePlayer(), bySomePlayer()).
                andAlsoIn(tiePositionsForTheOpponent(), byAnOpponent());
        given(theRules(board));
        thenTheRulesShouldDeclare(somePlayer(), aTie());
    }
    
    private RulesTest given(Board board) {
        this.board = board;
        return this;
    }
    
    private void given(Rules rules){
        this.rules = rules;
    }
    
    private void given(Movement movement){
        this.movement = movement;
    }

    private RulesTest whichHasBeenFilledIn(Collection<Integer> positions, Player owner) {
        for (int position : positions)
            board.fill(position, owner);
        return this;
    }

    private void whichHasBeenFilledIn(int position, Player owner) {
        board.fill(position, owner);
    }
    
    private void thenTheRulesShouldKnowTheGameIsOver() {
        assertTrue("The rules should know the game is over", rules.isOver());
    }

    private void thenTheRulesShouldKnowTheGameIsNotOver() {
        assertFalse("The rules should know the game isn't over", rules.isOver());
    }

    private void thenTheRulesShouldKnowTheMovementIsLegal() {
        assertTrue("The rules should know the movement is legal", 
                rules.isLegal(movement));
    }

    private void thenTheRulesShouldKnowTheMovementIsNotLegal() {
        assertFalse("The rules should know the movement isn't legal", 
                rules.isLegal(movement));
    }

    private void thenTheRulesShouldDeclare(Player player, Classification expected) {
        assertEquals("The player should know the right clasification for the player",
                expected, rules.classificationOf(player));
    }

    private Collection<Integer> tiePositionsForThePlayer() {
        Collection<Integer> tiePositions = new ArrayList<>();
        tiePositions.add(aPosition(ROW_UPPER, COLUMN_LEFT));
        tiePositions.add(aPosition(ROW_UPPER, COLUMN_RIGHT));
        tiePositions.add(aPosition(ROW_CENTER, COLUMN_LEFT));
        tiePositions.add(aPosition(ROW_CENTER, COLUMN_CENTER));
        tiePositions.add(aPosition(ROW_DOWN, COLUMN_CENTER));
        return tiePositions;
    }

    private void andAlsoIn(Collection<Integer> positions, Player owner) {
        whichHasBeenFilledIn(positions, owner);
    }

    private Collection<Integer> tiePositionsForTheOpponent() {
        Collection<Integer> tiePositions = new ArrayList<>();
        tiePositions.add(aPosition(ROW_UPPER, COLUMN_CENTER));
        tiePositions.add(aPosition(ROW_CENTER, COLUMN_RIGHT));
        tiePositions.add(aPosition(ROW_DOWN, COLUMN_LEFT));
        tiePositions.add(aPosition(ROW_DOWN, COLUMN_RIGHT));
        return tiePositions;
    }
    
}
