package com.obprado.tictactoe.model.game.service;

import com.obprado.tictactoe.model.game.entity.*;
import static com.obprado.tictactoe.model.game.TestFactories.*;
import java.util.*;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author omar
 */
public class TurnManagerTest {
    private TurnManager turnManager;
    private Map<Player, Integer> firstTurnFrecuencies;
    private Player firstPlayer;
    private Player secondPlayer;
    private Player turn;
    public final static int MAX_MOVEMENTS_POSSIBLE = 9;
    
    @Test
    public void shouldAssignFirstTurnFairly(){  
        given(somePlayer(), andADifferentPlayer());
        given(aTurnManager(firstPlayer, secondPlayer));
        whenItIsAskedForTheFirstTurn(aThousandTimes());
        thenEachPlayerShouldHaveBeenAsignedBetween(
                fourHundredTimes(), sixHundredTimes());
    }
    
    @Test
    public void shouldChangeTheFirstTimeItsPlayed(){
        given(somePlayer(), andADifferentPlayer());
        given(aTurnManager(firstPlayer, secondPlayer));
        whenItIsAskedForTheTurn();
        whenThePlayerHasMadeAMovement(turn);
        thenTheNextTurnShouldNotBelongTo(turn);
    }
    
    @Test
    public void shouldChangeEveryTimeItsPlayed(){
        given(somePlayer(), andADifferentPlayer());
        given(aTurnManager(firstPlayer, secondPlayer));
        whenItIsAskedForTheTurn();
        for (int movementsMade = 1; 
                movementsMade <= MAX_MOVEMENTS_POSSIBLE; 
                movementsMade++){
            whenThePlayerHasMadeAMovement(turn);
            thenTheNextTurnShouldNotBelongTo(turn);
            refreshTurn();
        }
    }
    
    private void given(Player playerOne, Player playerTwo) {
        this.firstPlayer = playerOne;
        this.secondPlayer = playerTwo;
    }

    private void given(TurnManager turnManager) {
        this.turnManager = turnManager;
    }

    private void whenItIsAskedForTheFirstTurn(int timesAsked) {
        firstTurnFrecuencies = new HashMap<>();
        firstTurnFrecuencies.put(firstPlayer, 0);
        firstTurnFrecuencies.put(secondPlayer, 0);
        Player asigned;
        for (int i = 1; i <= timesAsked; i++){
            turnManager.initiative();
            asigned = turnManager.getCurrentTurn();
            firstTurnFrecuencies.put(
                    asigned, 
                    firstTurnFrecuencies.get(asigned) + 1);
        }            
    }

    private void thenEachPlayerShouldHaveBeenAsignedBetween(
            int minimun, int maximun) {
        assertTrue("The frecuencies should be in the expected range", 
                firstTurnFrecuencies.get(firstPlayer) > minimun);
        assertTrue("The frecuencies should be in the expected range", 
                firstTurnFrecuencies.get(firstPlayer) < maximun);
        assertTrue("The frecuencies should be in the expected range", 
                firstTurnFrecuencies.get(secondPlayer) > minimun);
        assertTrue("The frecuencies should be in the expected range", 
                firstTurnFrecuencies.get(secondPlayer) < maximun);
    }

    private void whenItIsAskedForTheTurn() {
        this.turn = turnManager.getCurrentTurn();
    }

    private void whenThePlayerHasMadeAMovement(Player turn) {
        Movement movement = turn.decideMovementFor(anEmptyBoard(sized(3)));
        turnManager.notifyA(movement);
    }

    private void thenTheNextTurnShouldNotBelongTo(Player unexpectedTurn) {
        assertNotEquals("The turn manager should know how is the next player", 
                unexpectedTurn, turnManager.getCurrentTurn());
    }

    private void refreshTurn() {
        this.turn = turnManager.getCurrentTurn();
    }
    
}
