package com.obprado.tictactoe.model.player;

import com.obprado.tictactoe.model.game.entity.*;

/**
 *
 * @author omar
 */
public class FreePositionDisplay extends MockDisplay{

    @Override
    public int askForAPosition(Board board) {
        return board.freePositions().iterator().next();
    }
    
}
