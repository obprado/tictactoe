package com.obprado.tictactoe.model.player;

import com.obprado.tictactoe.model.game.entity.*;
import static com.obprado.tictactoe.model.game.TestFactories.*;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author omar
 */
public class HumanPlayerTest {
    private PlayerDisplay display;
    private Player player;
    private Movement movement;
    
    @Test
    public void shouldAskTheDisplayForAMovement(){
        given(aDisplay());
        given(aHumanThatInteractsThroughA(display));
        whenItIsAskedForAMovementOn(anEmptyBoard(sized(3)));
        thenItShouldMoveTheSameAs(aDisplay());
    }

    private void given(PlayerDisplay display) {
        this.display = display;
    }
    
    private void given(HumanPlayer player) {
        this.player = player;
    }

    private void whenItIsAskedForAMovementOn(Board board) {
        this.movement = player.decideMovementFor(board);
    }

    private void thenItShouldMoveTheSameAs(PlayerDisplay display) {
        assertEquals("The player's movement should come from the display",
                display.askForAPosition(anEmptyBoard(sized(3))), movement.getPosition());
    }
    
}
