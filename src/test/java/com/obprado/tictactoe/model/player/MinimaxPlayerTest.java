package com.obprado.tictactoe.model.player;

import java.util.*;
import com.obprado.tictactoe.model.game.entity.*;
import org.junit.*;
import static org.junit.Assert.*;
import static com.obprado.tictactoe.model.game.TestFactories.*;

/**
 *
 * @author omar
 */
public class MinimaxPlayerTest {
    private Player player;    
    private Board board;
    private Movement movement;
    
    @Before
    public void setUp(){
        this.player = new MinimaxPlayer(byAnOpponent());
    }
    
    @Test
    public void shouldPreventEnemyFromRowVictory(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(fourPositions(), byAnOpponent()).
                andAlsoIn(threePositions(), player);
        whenThePlayerIsAskedForAMovement();
        thenTheMovementShouldBeIn(aPosition(ROW_CENTER, COLUMN_RIGHT));
    }
    
    @Test
    public void shouldWinIfPossible(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(twoAscendentCorners(), player).
                andAlsoIn(twoDescendentCorners(), byAnOpponent());
        whenThePlayerIsAskedForAMovement();
        thenTheMovementShouldBeIn(theCenter());
    }
    
    @Test
    public void shouldPreventTheEnemyFromColumnVictory(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(theCenter(), player).
                andAlsoIn(twoPositionsInAColumn(COLUMN_LEFT), byAnOpponent());
        whenThePlayerIsAskedForAMovement();
        thenTheMovementShouldBeIn(aPosition(ROW_CENTER, COLUMN_LEFT));
    }
    
    @Test
    public void shouldBeginInTheCenterOrACorner(){
        //If you start on a side, a perfect opponent will always win you
        given(anEmptyBoard(sized(3)));
        whenThePlayerIsAskedForAMovement();
        thenTheMovementShouldBeInOneOf(centerAndCorners());
    }
    
    @Test
    public void shouldPreventAFork(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(cornersInColumn(COLUMN_LEFT), byAnOpponent()).
                andAlsoIn(aPosition(ROW_CENTER, COLUMN_LEFT), player);
        whenThePlayerIsAskedForAMovement();
        thenTheMovementShouldBeIn(theCenter());
    }
    
    @Test
    public void shouldCreateAFork(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(topAndCenter(), byAnOpponent()).
                andAlsoIn(downAndCorner(), player);
        whenThePlayerIsAskedForAMovement();
        thenTheMovementShouldBeIn(aPosition(ROW_DOWN, COLUMN_RIGHT));
    }
    
    @Test
    public void shouldNotBeInvokedWithAFullBoard(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(allThePositions(), byAnOpponent());
        thenAskingForAMovementShouldFailWith(MinimaxPlayer.FULL_BOARD);
    }
    
    @Test
    public void shouldNotBeInvokedWithAnOverGame(){
        given(anEmptyBoard(sized(3))).
                whichHasBeenFilledIn(alignedTrio(), byAnOpponent());
        thenAskingForAMovementShouldFailWith(MinimaxPlayer.GAME_OVER);
    }

    private MinimaxPlayerTest given(Board board) {
        this.board = board;
        return this;
    }
    
    private MinimaxPlayerTest whichHasBeenFilledIn(Collection<Integer> positions, Player owner) {
        for (int position : positions)
            board.fill(position, owner);
        return this;
    }
    
    private MinimaxPlayerTest whichHasBeenFilledIn(int position, Player owner) {
        board.fill(position, owner);   
        return this;
    }
    
    private void andAlsoIn(Collection<Integer> positions, Player owner) {
        whichHasBeenFilledIn(positions, owner);
    }
    
    private void andAlsoIn(int position, Player owner) {
        whichHasBeenFilledIn(position, owner);
    }
    
    private Collection<Integer> fourPositions(){
        Collection<Integer> positions = new ArrayList<>();
        positions.add(aPosition(ROW_UPPER, COLUMN_LEFT));
        positions.add(aPosition(ROW_CENTER, COLUMN_LEFT));
        positions.add(aPosition(ROW_CENTER, COLUMN_CENTER));
        positions.add(aPosition(ROW_DOWN, COLUMN_CENTER));
        return positions;
    }
    
    private Collection<Integer> threePositions(){
        Collection<Integer> positions = new ArrayList<>();
        positions.add(aPosition(ROW_UPPER, COLUMN_CENTER));
        positions.add(aPosition(ROW_DOWN, COLUMN_LEFT));
        positions.add(aPosition(ROW_DOWN, COLUMN_RIGHT));
        return positions;
    }

    private void whenThePlayerIsAskedForAMovement() {
        movement = player.decideMovementFor(board);
    }

    private void thenTheMovementShouldBeIn(int expected) {
        assertEquals("The movement should be in the expected position", 
                expected, movement.getPosition());
    }

    private void thenTheMovementShouldBeInOneOf(Collection<Integer> acceptablePositions) {
        assertTrue("The movement should be in one of the expected positions", 
                acceptablePositions.contains(movement.getPosition()));
    }

    private Collection<Integer> downAndCorner() {
        Collection<Integer> positions = new ArrayList<>();
        positions.add(aPosition(ROW_DOWN, COLUMN_CENTER));
        positions.add(aPosition(ROW_UPPER, COLUMN_RIGHT));
        return positions;
    }

    private void thenAskingForAMovementShouldFailWith(String errorMessage) {
        try {
            player.decideMovementFor(board);
            fail("Asking for a movement should fail");
        } catch (IllegalArgumentException exception){
            assertEquals("The exception should inform about the cause of the problem", 
                    errorMessage, exception.getMessage());
        }
    }
    
}
