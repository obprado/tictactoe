package com.obprado.tictactoe.model.player;

import static com.obprado.tictactoe.model.game.TestFactories.*;
import com.obprado.tictactoe.model.game.entity.*;

/**
 *
 * @author omar
 */
public class MockDisplay implements PlayerDisplay{
    private boolean notifiedInvalid;
    private boolean notifiedEndOfGame;
    private boolean notifiedMovement;
    private int timesAskedForAPosition;
    private final int firstPosition = aPosition(ROW_DOWN, COLUMN_CENTER);
    private final int secondPosition = aPosition(ROW_DOWN, COLUMN_RIGHT);

    public MockDisplay() {
        notifiedInvalid = false;
        notifiedEndOfGame = false;
        notifiedMovement = false;
        timesAskedForAPosition = 0;
    }

    @Override
    public int askForAPosition(Board board) {
        timesAskedForAPosition++;
        if (timesAskedForAPosition == 1)
            return firstPosition;
        else
            return secondPosition;
    }

    @Override
    public void notifyInvalid(Movement movement) {
        notifiedInvalid = true;
    }

    public boolean notifiedInvalid(){
        return notifiedInvalid;
    }

    public int timesAskedForAPosition(){
        return timesAskedForAPosition;
    }

    public int firstPosition(){
        return firstPosition;
    }

    public int secondPosition(){
        return secondPosition;
    }

    @Override
    public void notifyEndOfGame() {
        notifiedEndOfGame = true;
    }

    public boolean notifiedEndOfGame() {
        return notifiedEndOfGame;
    }

    @Override
    public void notifyMovement() {
        this.notifiedMovement = true;
    }

    public boolean isNotifiedMovement() {
        return notifiedMovement;
    }

}
