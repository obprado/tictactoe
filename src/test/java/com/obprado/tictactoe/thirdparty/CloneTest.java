package com.obprado.tictactoe.thirdparty;

import com.rits.cloning.Cloner;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author omar
 */
public class CloneTest {
    
    class Person{
        private String name;
        public Person(String name) {this.name = name;}
        public void setName(String name) {this.name = name;}
        public String getName() {return name;}
    }

    @Test
    public void notCloningShouldCreateProblems(){
        Person john = new Person("John");
        Person laura = john;
        laura.setName("laura");
        assertEquals("The object should have the wrong name", "laura", john.getName());
    }
    
    @Test
    public void cloningShouldPreventOverwriting() throws CloneNotSupportedException{
        Person john = new Person("John");
        Person laura = new Cloner().deepClone(john);
        laura.setName("laura");
        assertEquals("The object should have the initial name", "John", john.getName());
    }
    
}
