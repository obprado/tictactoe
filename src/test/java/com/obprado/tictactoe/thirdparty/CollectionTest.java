package com.obprado.tictactoe.thirdparty;

import com.obprado.tictactoe.model.game.entity.*;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.*;

/**
 *
 * @author omar
 */
public class CollectionTest {
    
    @Test
    public void arrayAsListShouldFailToRemove(){
        try {
            Arrays.asList( "some element", "another element" ).remove("some element");
            fail("Altering a collection obtained with 'Arrays.asList' shouldn't be allowed");
        } catch (UnsupportedOperationException unsupportedException){
            
        }
    }
    
    @Test
    public void newArrayAsListShouldWorkFine(){
        try {
            new ArrayList(Arrays.asList("value", "another value")).remove("value");
        } catch(RuntimeException exception){
            fail("New collection filled from Arrays.asList should be alterable");
        }
    }
    
}
