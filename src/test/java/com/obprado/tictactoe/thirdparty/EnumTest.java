package com.obprado.tictactoe.thirdparty;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author omar
 */
public class EnumTest {
    
    public enum Day {
        MONDAY, TUESDAY,  WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
    }
    
    @Test
    public void testComparison(){
        assertTrue("comparison don't work with doble equal sign", Day.MONDAY == Day.MONDAY);
        assertTrue("comparison don't work with equal()", Day.MONDAY.equals(Day.MONDAY));        
        assertEquals("assertEquals() don't work with enums", Day.MONDAY, Day.MONDAY);
    }
    
}
