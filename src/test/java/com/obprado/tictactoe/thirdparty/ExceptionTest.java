package com.obprado.tictactoe.thirdparty;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author omar
 */
public class ExceptionTest {
    public static final String ERROR_MESSAGE = "some error message";
    
    @Test
    public void testGetMessageOfRuntimeException(){
        try {
            throw new RuntimeException(ERROR_MESSAGE);
        } catch(RuntimeException exception){
            assertEquals("The error message in the exception constructor should match the getMessage() method", 
                    ERROR_MESSAGE, exception.getMessage());
        }
    }
}
