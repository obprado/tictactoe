package com.obprado.tictactoe.ui.terminal;

import com.obprado.tictactoe.model.game.TestFactories;
import java.util.*;
import com.obprado.tictactoe.model.game.entity.*;
import static com.obprado.tictactoe.model.game.TestFactories.*;
import static com.obprado.tictactoe.ui.terminal.BoardPrinter.*;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author omar
 */
public class BoardPrinterTest {
    private BoardPrinter printer;
    private Board board;
    private PlayerPrinter humanPrinter;
    private PlayerPrinter computerPrinter;
    private MockScreen screen;
    
    @Test
    public void shouldPrintABoard(){
        given(aScreen());
        given(anEmptyBoard(sized(3))).
                filledIn(cornersInRow(TestFactories.ROW_UPPER), bySomePlayer()).
                andAlsoIn(theCenter(), byAnOpponent());
        givenAHumanPrinter(aPlayerPrinter(ofSomePlayer(), 'X'));
        givenAComputerPrinter(aPlayerPrinter(byAnOpponent(), 'O'));
        given(aBoardPrinter(board, humanPrinter, computerPrinter, screen));
        whenItIsAskedToPrintTheBoard();
        thenTheScreenShouldHaveSeen(CEILING, SPACING, aTopRow(), FLOOR, aMidRow());
    }

    private BoardPrinterTest given(Board board) {
        this.board = board;
        return this;
    }

    private BoardPrinterTest filledIn(Collection<Integer> filled, Player owner) {
        for (int position : filled)
            board.fill(position, owner);
        return this;
    }

    private void andAlsoIn(int filled, Player owner) {
        board.fill(filled, owner);
    }
    
    private void givenAHumanPrinter(PlayerPrinter printer) {
        this.humanPrinter = printer;
    }

    private void givenAComputerPrinter(PlayerPrinter printer) {
        this.computerPrinter = printer;
    }
    
    private void given(MockScreen screen) {
        this.screen = screen;
    }
    
    private void given(BoardPrinter printer) {
        this.printer = printer;
    }

    private BoardPrinter aBoardPrinter(Board board, PlayerPrinter humanPrinter, PlayerPrinter computerPrinter, MockScreen screen) {
        return new BoardPrinter(board, screen, humanPrinter, computerPrinter);
    }

    private void whenItIsAskedToPrintTheBoard() {
        printer.printBoard();
    }
    
    private String aTopRow(){
        return "|" + "  X  " + "|" + "     " + "|" + "  X  " + "|";
    }
    
    private String aMidRow(){
        return "|" + "     " + "|" + "  O  " + "|" + "     " + "|";
    }

    private void thenTheScreenShouldHaveSeen(String... expectedLines) {
        for (String line : expectedLines)
            assertTrue("The screen should have seen all the expected lines", 
                    screen.getPrinted().contains(line));
    }
    
}
