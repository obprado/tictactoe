package com.obprado.tictactoe.ui.terminal;

import static com.obprado.tictactoe.model.game.TestFactories.*;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author omar
 */
public class FreePositionPrinterTest {
    
    @Test
    public void shouldKnowItCanPrintOnAnEmptySpace(){
        assertTrue("The printer should know it can print", 
                new FreePositionPrinter().canPrint(theCenter(), anEmptyBoard(sized(3))));
    }
    
    @Test
    public void shouldPrintAnEmptySpace(){
        assertEquals("The printer should print an empty space character", 
                ' ', new FreePositionPrinter().getSymbol());
    }
    
}
