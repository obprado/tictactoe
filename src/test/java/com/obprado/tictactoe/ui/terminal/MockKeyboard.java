package com.obprado.tictactoe.ui.terminal;

import java.io.*;
import java.util.*;

/**
 *
 * @author omar
 */
public class MockKeyboard extends BufferedReader{
    private Collection<String> inputs;
    private int timesReadLine;
    private int cableDisconnections;
    public static final String SYSTEM_ERROR = "Random error from the system. This shouldn't be printed on the screen";

    public MockKeyboard(Reader in) {
        super(in);
    }

    public void setInputs(Collection<String> inputs) {
        this.inputs = inputs;
    }

    @Override
    public String readLine() throws IOException {
        if (cableDisconnections > timesReadLine){
            timesReadLine++;
            throw new IOException(SYSTEM_ERROR);
        }
        timesReadLine++;
        Iterator<String> iterator = inputs.iterator();
        String first = iterator.next();
        iterator.remove();
        return first;
    }

    int timesReadLine() {
        return timesReadLine;
    }

    void broken(int timesToShowTheProblem) {
        this.cableDisconnections = timesToShowTheProblem;
    }


}
