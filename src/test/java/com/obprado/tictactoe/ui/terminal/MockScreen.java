package com.obprado.tictactoe.ui.terminal;

import java.io.*;
import java.util.*;

/**
 *
 * @author omar
 */
public class MockScreen extends PrintStream{
    private final Collection<String> printed;

    public MockScreen(OutputStream out) {
        super(out);
        printed = new ArrayList<>();
    }

    @Override
    public void println(String toPrint) {
        printed.add(toPrint);
    }

    public Collection<String> getPrinted() {
        return printed;
    }
    
}
