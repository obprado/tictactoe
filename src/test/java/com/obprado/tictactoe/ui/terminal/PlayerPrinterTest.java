package com.obprado.tictactoe.ui.terminal;

import com.obprado.tictactoe.model.game.entity.*;
import static com.obprado.tictactoe.model.game.TestFactories.*;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author omar
 */
public class PlayerPrinterTest {
    private PlayerPrinter playerPrinter;
    
    @Test
    public void shouldPrintTheGivenSymbol(){
        given(aPlayerPrinter(ofSomePlayer(), whichSymbolIs('X')));
        thenItShouldPrint('X');
    }
    
    @Test
    public void shouldKnowIfItCanPrintForAPlayer(){
        given(aPlayerPrinter(ofSomePlayer(), whichSymbolIs('Y')));
        thenItShouldKnowItCanPrintFor(somePlayer());
    }
    
    @Test
    public void shouldKnowIfItCanPrintForAPosition(){
        given(aPlayerPrinter(ofSomePlayer(), whichSymbolIs('Y')));
        thenItShouldKnowItCanPrintFor(
                theCenter(), 
                filledBoard(theCenter(), ofSomePlayer()));
    }
    
    private void given(PlayerPrinter printer) {
        this.playerPrinter = printer;
    }
    
    private char whichSymbolIs(char symbol){
        return symbol;
    }

    private void thenItShouldPrint(char expected) {
        assertEquals("The player should print it's symbol", expected, playerPrinter.getSymbol());
    }

    private void thenItShouldKnowItCanPrintFor(Player player) {
        assertTrue("The printer should recognize it's player", 
                playerPrinter.belongsTo(player));
    }
    
    private Board filledBoard(int position, Player player){
        Board board = new Board(3);
        board.fill(position, player);
        return board;
    }

    private void thenItShouldKnowItCanPrintFor(int position, Board board) {
        assertTrue("The printer should know it can print in the position",
                playerPrinter.canPrint(position, board));
    }
    
}
