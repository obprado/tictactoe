package com.obprado.tictactoe.ui.terminal;

import java.io.*;
import java.util.*;
import com.obprado.tictactoe.model.game.entity.*;
import com.obprado.tictactoe.application.*;
import com.obprado.tictactoe.model.game.TestFactories;
import com.obprado.tictactoe.model.game.service.Classification;
import static com.obprado.tictactoe.model.game.TestFactories.*;
import com.obprado.tictactoe.model.player.*;
import static com.obprado.tictactoe.ui.terminal.BoardPrinter.*;
import static com.obprado.tictactoe.ui.terminal.TerminalDisplay.*;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author omar
 */
public class TerminalDisplayTest {
    private MockScreen screen;
    private MockKeyboard keyboard;
    private int position;
    private TerminalDisplay controller;
    private MockUseCase useCase;
    
    public static final String LEFT = "L";
    public static final String LEFT_DOWNCASE = "l";
    public static final String DOWN = "D";
    public static final String DOWN_DOWNCASE = "d";
    public static final String CENTER = "C";
    public static final String CENTER_DOWNCASE = "c";
    public static final String INVALID = "JFKLDS";
    public static final String EMPTY = "";    
    
    public static final String UPPER_LEFT = "1";    
    public static final String UPPER_CENTER = "2";    
    public static final String UPPER_RIGHT = "3";    
    public static final String CENTER_LEFT = "4";
    public static final String CENTER_CENTER = "5";
    public static final String CENTER_RIGHT = "6";
    public static final String DOWN_LEFT = "7";    
    public static final String DOWN_CENTER = "8";    
    public static final String DOWN_RIGHT = "9";    
    
    @Test
    public void shouldReadACornerFromTheKeyboard(){
        given(aScreen());   
        given(aKeyboard(willingToWrite(DOWN_LEFT)));
        given(aTerminalDisplay(keyboard, screen));
        whenTheDisplayIsAskedForAPosition();
        thenItShouldReturn(aPosition(
                TestFactories.ROW_DOWN, TestFactories.COLUMN_LEFT));
    }
    
    @Test
    public void shouldReadTheCenterFromTheKeyboard(){
        given(aScreen());
        given(aKeyboard(willingToWrite(CENTER_CENTER)));
        given(aTerminalDisplay(keyboard, screen));
        whenTheDisplayIsAskedForAPosition();
        thenItShouldReturn(theCenter());
    }
    
    @Test
    public void shouldDetectInvalidInput(){
        given(aScreen());
        given(aKeyboard(willingToWrite(INVALID, CENTER_CENTER)));
        given(aTerminalDisplay(keyboard, screen));
        whenTheDisplayIsAskedForAPosition();
        thenItShouldAskForAnotherPosition();
        thenTheUserShouldHaveSeen(REQUEST_POSITION, 
                INVALID_POSITION, 
                REQUEST_POSITION);
    }
    
    @Test
    public void shouldExplainTheUserWhenItNeedsInput(){
        given(aScreen());
        given(aKeyboard(willingToWrite(DOWN_LEFT)));
        given(aTerminalDisplay(keyboard, screen));
        whenTheDisplayIsAskedForAPosition();
        thenTheUserShouldHaveSeen(REQUEST_POSITION);
    }
    
    @Test
    public void shouldTellTheUserWhenThePositionIsNotValid(){
        given(aScreen());
        given(aKeyboard(willingToWrite()));
        given(aTerminalDisplay(keyboard, screen));
        whenTheDisplayIsNotifiedInvalid();
        thenTheUserShouldHaveSeen(FREE_POSITION);
    }
    
    @Test
    public void shouldNotifyEndOfTheGame(){
        given(aScreen());
        given(aKeyboard(willingToWrite()));        
        given(aMockGame()).
                willingToDeclare(somePlayer(), Classification.UNCLASSIFIED);
        given(aTerminalDisplay(keyboard, screen, useCase));
        whenTheDisplayIsNotifiedEndOfGame();
        thenTheUserShouldHaveSeen(GAME_OVER);
    }
    
    @Test
    public void shouldNotifyVictory(){
        given(aScreen());
        given(aKeyboard(willingToWrite()));        
        given(aMockGame()).
                willingToDeclare(somePlayer(), Classification.WINNER);
        given(aTerminalDisplay(keyboard, screen, useCase));
        whenTheDisplayIsNotifiedEndOfGame();
        thenTheUserShouldHaveSeen(CONGRATULATIONS);
    }
    
    @Test
    public void shouldNotifyDefeat(){
        given(aScreen());
        given(aKeyboard(willingToWrite()));
        given(aMockGame()).
                willingToDeclare(somePlayer(), Classification.LOOSER);
        given(aTerminalDisplay(keyboard, screen, useCase));
        whenTheDisplayIsNotifiedEndOfGame();
        thenTheUserShouldHaveSeen(DEFEAT);
    }
    
    @Test
    public void shouldNotifyTie(){
        given(aScreen());
        given(aKeyboard(willingToWrite()));
        given(aMockGame()).
                willingToDeclare(somePlayer(), Classification.TIE);
        given(aTerminalDisplay(keyboard, screen, useCase));        
        whenTheDisplayIsNotifiedEndOfGame();
        thenTheUserShouldHaveSeen(DRAW);
    }
    
    @Test
    public void shouldTellWhenTheKeyboardFails(){
        given(aScreen());
        given(aKeyboard(willingToWrite(DOWN_LEFT, DOWN_LEFT))).
                whichCableDisconnects(oneTime());
        given(aTerminalDisplay(keyboard, screen));
        whenTheDisplayIsAskedForAPosition();
        thenTheUserShouldHaveSeen(KEYBOARD_PROBLEM);
    }
    
    @Test
    public void shouldNotifyAMovementWhenExecuted(){
        given(aScreen());
        given(aKeyboard(willingToWrite(DOWN_LEFT)));
        given(aMockGame()).
                withABoard(filledIn(theCenter(), byTheHumanPlayer()));
        given(aTerminalDisplay(keyboard, screen, useCase));
        whenTheDisplayIsNotifiedAMovement();
        thenTheUserShouldHaveSeen(CEILING, SPACING, FLOOR, aMidRow());
    }
    
    @Test
    public void shouldCreateAHumanPlayer(){
        given(aScreen());
        given(aKeyboard(willingToWrite()));
        given(aTerminalDisplay(keyboard, screen));
        thenTheDisplayShouldHaveAPlayer();
        thenTheDisplaysPlayerShouldBeHuman();        
    }
    
    @Test
    public void shouldAcceptDowncaseInput(){
        given(aScreen());
        given(aKeyboard(willingToWrite(CENTER_LEFT)));
        given(aMockGame());
        given(aTerminalDisplay(keyboard, screen));
        whenTheDisplayIsAskedForAPosition();
        thenItShouldNotAskForAnotherPosition();
    }

    private void given(MockScreen aScreen) {
        this.screen = aScreen;
    }

    private TerminalDisplay aTerminalDisplay(
            MockKeyboard keyboard, MockScreen screen) {
        return new TerminalDisplay(keyboard, screen);
    }
    
    private void given(TerminalDisplay display) {
        this.controller = display;
    }

    private MockKeyboard aKeyboard(Collection<String> inputs) {
        MockKeyboard mockKeyboard = new MockKeyboard(new InputStreamReader(System.in));
        mockKeyboard.setInputs(inputs);
        return mockKeyboard;
    }
    
    private TerminalDisplayTest given(MockKeyboard keyboard) {
        this.keyboard = keyboard;
        return this;
    }

    private void whenTheDisplayIsAskedForAPosition() {
        position = controller.askForAPosition(anEmptyBoard(sized(3)));
    }

    private void thenItShouldReturn(int expected) {
        assertEquals("The controller should return the expected position",
                expected, position);
    }

    private Collection<String> willingToWrite(String... keyboardInputs) {
        Collection<String> inputs = new ArrayList<>();
        inputs.addAll(Arrays.asList(keyboardInputs));
        return inputs;
    }

    private void thenItShouldAskForAnotherPosition() {
        assertEquals("The Terminal display should have asked for another position", 
                2, keyboard.timesReadLine());
    }

    private void thenTheUserShouldHaveSeen(String... expectedLines) {
        for (String line : expectedLines)
            assertTrue("The user should have seen the expected lines", 
                screen.getPrinted().contains(line));
    }

    private void whenTheDisplayIsNotifiedInvalid() {
        controller.notifyInvalid(aMovement(theCenter(), somePlayer()));
    }

    private void whenTheDisplayIsNotifiedEndOfGame() {
        controller.notifyEndOfGame();
    }
    
    private TerminalDisplayTest given(MockUseCase useCase) {
        this.useCase = useCase;
        return this;
    }

    private MockUseCase aMockGame() {
        return new MockUseCase(controller, aTurnManager(somePlayer(), byAnOpponent()));
    }

    private void willingToDeclare(Player player, Classification clasification) {
        this.useCase.setClasification(clasification);
    }
    
    private int oneProblem(){
        return 1;
    }

    private void whichCableDisconnects(int timesToShowTheProblem) {
        keyboard.broken(timesToShowTheProblem);
    }

    private void whenTheDisplayIsNotifiedAMovement() {
        controller.notifyMovement();
    }
    
    private String aMidRow(){
        return "|" + "     " + "|" + "  X  " + "|" + "     " + "|";
    }

    private Board filledIn(int position, Player player){
        Board board = new Board(3);
        board.fill(position, player);        
        return board;
    }

    private void withABoard(Board board) {
        useCase.setBoard(board); 
    }

    private Player byTheHumanPlayer() {
        return useCase.getHumanPlayer();
    }

    private TerminalDisplay aTerminalDisplay(
            MockKeyboard keyboard, MockScreen screen, MockUseCase useCase) {
        return new TerminalDisplay(useCase, screen, keyboard);
    }

    private void thenTheDisplayShouldHaveAPlayer() {
        assertNotNull("The player should not be null", controller.getPlayer());
    }

    private void thenTheDisplaysPlayerShouldBeHuman() {
        assertTrue("The display's player should be human", controller.getPlayer() instanceof HumanPlayer);
    }

    private void thenItShouldNotAskForAnotherPosition() {
        assertEquals("The Terminal display should have asked for another position", 
                1, keyboard.timesReadLine());
    }
    
}
